#!/bin/bash
chromium \
  --headless \
  --disable-gpu \
  --print-to-pdf-no-header \
  --print-to-pdf=index.pdf \
  index.html

cp plot.pdf miR200ab429.correlation.pdf
cp .RData miR200ab429.RData

zip \
  "Practical 1.zip" \
  miR200ab429.correlation.pdf \
  miR200ab429.RData