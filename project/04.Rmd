# Gene Set Enrichment Analysis

Gene set enrichment analysis
and
gene set overrepresentation analysis of
discovery and validation set
was conducted in R\ [@rcoreteamLanguageEnvironmentStatistical2021]
using HTSanalyzeR2\ [@linaHTSanalyzeR22020].
See Programming Code's [Step 4] for details.
