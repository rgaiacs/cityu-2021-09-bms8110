# Step 2 {-}

Let's load the libraries that we will use in this step.

```{r message=FALSE, warning=FALSE}
library(tidyverse)

library(dendextend)

library(ggrepel)

library(affy)
```

```{r message=FALSE, warning=FALSE}
package.version('tidyverse')
package.version('dendextend')
package.version('ggrepel')
package.version('affy')
```

## Preprocess of Discovery Set {-}

Let's calculate gene expression levels
using Robust Multi-Array (RMA) Average expression measure.

```{r}
discovery_expression <- discovery_set %>%
  rma() 
```

And
let's visualise the gene expression levels.

(ref:discovery-box-plot) Box plot visualisation for discovery set.

```{r discovery-box-plot, fig.cap = "(ref:discovery-box-plot)", fig.show = 'hold', fig.align = 'center'}
discovery_expression %>%
  exprs() %>%
  as.data.frame() %>%
  stack() %>%
  ggplot(
    aes(x = ind, y = values)
  ) +
  geom_boxplot() +
  theme(axis.text.x = element_text(angle = 90)) +
  ggtitle('Sample Distributions') +
  xlab('Samples') +
  ylab('Log2 rma signal')
```

Based on Figure\ \@ref(fig:discovery-box-plot),
we can verify that the distributions of gene expression across
samples are extremely similar.

## Quality Control of Discovery Set {-}

Let's check for physical defects in the arrays.

```{r}
discovery_set %>% image()
```

All arrays are absent of physical defects.

Let's create a dendrogram
based on hierarchical clustering.

(ref:discovery-dendrogram) Dendrogram for discovery set. GEO accession numbers in blue are normal squamous cervical epitheilia (control) samples and in red are high grade squamous intraepithelial lesions (case).

```{r discovery-dendrogram, fig.cap = "(ref:discovery-dendrogram)", fig.show = 'hold', fig.align = 'center'}
f.distCol.Pearson <- function(x.mx) {
  dist.mx <- 1 - cor(x.mx, method = "pearson")
  return(as.dist (dist.mx))
}

dend <- discovery_set %>%
  exprs() %>%
  f.distCol.Pearson() %>%
  hclust() %>%
  as.dendrogram()

normal_ids <- rownames(phenoData(discovery_set))[phenoData(discovery_set)$status == 'NC']
label_colour <- labels(dend) %>%
  sapply(
    function(dend_label) {
      if (dend_label %in% normal_ids) {
        c <- 'blue'
      }
      else {
        c <- 'red'
      }
      return(c)
    }
  )

dend %>%
  set(
    "labels_col",
    label_colour
  ) %>%
  plot(
    main = 'Hierarchical Clustering',
    # sub = '1 - Pearson Correlation',
    # xlab = 'Samples',
    ylab = 'Distance'
  )
```

Based on Figure\ \@ref(fig:discovery-dendrogram),
we say that classes are generally well separated.

Let's calculate principal component analysis (PCA)
for our data.

```{r}
pca <- discovery_set %>%
  exprs() %>%
  t() %>%
  prcomp()
```

Let's visualise the variance in our data
based in the calculated principal components.

(ref:discovery-pca-variance) Variance in discovery set as function of principal components.

```{r discovery-pca-variance, fig.cap = "(ref:discovery-pca-variance)", fig.show = 'hold', fig.align = 'center'}
eigs <- pca$sdev^2

varexplained <- eigs / sum(eigs)

varexplained_df = data.frame(
  x=1:length(varexplained),
  y=varexplained * 100
)

varexplained_df %>%
  ggplot(
    aes(x=x, y=y)
  ) +
  geom_bar(stat = "identity") +
  xlab('Principal component') +
  ylab('% variance explained')
```

Let's visualise the projection of our data
in the space of the first and second principal component.

(ref:discovery-pca-projection) Data in the space of the first and second principal component

```{r discovery-pca-projection, fig.cap = "(ref:discovery-pca-projection)", fig.show = 'hold', fig.align = 'center'}
pca_df <- data.frame(pca$x)
pca_df$sample <- pca_df %>%
  rownames()
pca_df$status <- pca_df %>%
  rownames() %>%
  sapply(
    function(rown_name) {
      if (rown_name %in% normal_ids) {
        c <- 'NC'
      }
      else {
        c <- 'CC'
      }
      return(c)
    }
  ) %>%
  as.factor() 

pca_df %>%
  ggplot(
    aes(x=PC1, y=PC2)
  ) +
  geom_point(aes(colour=status)) +
  geom_text_repel(aes(label=sample)) +
  xlab('PC1') +
  ylab('PC2')
  
```

Based on Figure\ \@ref(fig:discovery-pca-projection),
we say that classes are well separated.

## Preprocess of Validation Set {-}

Let's calculate gene expression levels
using Robust Multi-Array (RMA) Average expression measure.

```{r}
validation_expression <- validation_set %>%
  rma() 
```

And
let's visualise the gene expression levels.

(ref:validation-box-plot) Box plot visualisation for validation set.

```{r validation-box-plot, fig.cap = "(ref:validation-box-plot)", fig.show = 'hold', fig.align = 'center'}
validation_expression %>%
  exprs() %>%
  as.data.frame() %>%
  stack() %>%
  ggplot(
    aes(x = ind, y = values)
  ) +
  geom_boxplot() +
  theme(axis.text.x = element_text(angle = 90)) +
  ggtitle('Sample Distributions') +
  xlab('Samples') +
  ylab('Log2 rma signal')
```

Based on Figure\ \@ref(fig:validation-box-plot),
we can verify that the distributions of gene expression across
samples are extremely similar.

## Quality Control of Validation Set {-}

Let's check for physical defects in the arrays.

```{r}
validation_set %>% image()
```

All arrays are absent of physical defects.

Let's create a dendrogram
based on hierarchical clustering.

(ref:validation-dendrogram) Dendrogram for validation set. GEO accession numbers in blue are normal squamous cervical epitheilia (control) samples and in red are high grade squamous intraepithelial lesions (case).

```{r validation-dendrogram, fig.cap = "(ref:validation-dendrogram)", fig.show = 'hold', fig.align = 'center'}
f.distCol.Pearson <- function(x.mx) {
  dist.mx <- 1 - cor(x.mx, method = "pearson")
  return(as.dist (dist.mx))
}

dend <- validation_set %>%
  exprs() %>%
  f.distCol.Pearson() %>%
  hclust() %>%
  as.dendrogram()

normal_ids <- rownames(phenoData(validation_set))[phenoData(validation_set)$status == 'NC']
label_colour <- labels(dend) %>%
  sapply(
    function(dend_label) {
      if (dend_label %in% normal_ids) {
        c <- 'blue'
      }
      else {
        c <- 'red'
      }
      return(c)
    }
  )

dend %>%
  set(
    "labels_col",
    label_colour
  ) %>%
  plot(
    main = 'Hierarchical Clustering',
    # sub = '1 - Pearson Correlation',
    # xlab = 'Samples',
    ylab = 'Distance'
  )
```

Based on Figure\ \@ref(fig:validation-dendrogram),
we say that classes are generally well separated.

Let's calculate principal component analysis (PCA)
for our data.

```{r}
pca <- validation_set %>%
  exprs() %>%
  t() %>%
  prcomp()
```

Let's visualise the variance in our data
based in the calculated principal components.

(ref:validation-pca-variance) Variance in validation set as function of principal components.

```{r validation-pca-variance, fig.cap = "(ref:validation-pca-variance)", fig.show = 'hold', fig.align = 'center'}
eigs <- pca$sdev^2

varexplained <- eigs / sum(eigs)

varexplained_df = data.frame(
  x=1:length(varexplained),
  y=varexplained * 100
)

varexplained_df %>%
  ggplot(
    aes(x=x, y=y)
  ) +
  geom_bar(stat = "identity") +
  xlab('Principal component') +
  ylab('% variance explained')
```

Let's visualise the projection of our data
in the space of the first and second principal component.

(ref:validation-pca-projection) Data in the space of the first and second principal component

```{r validation-pca-projection, fig.cap = "(ref:validation-pca-projection)", fig.show = 'hold', fig.align = 'center'}
pca_df <- data.frame(pca$x)
pca_df$sample <- pca_df %>%
  rownames()
pca_df$status <- pca_df %>%
  rownames() %>%
  sapply(
    function(rown_name) {
      if (rown_name %in% normal_ids) {
        c <- 'NC'
      }
      else {
        c <- 'CC'
      }
      return(c)
    }
  ) %>%
  as.factor() 

pca_df %>%
  ggplot(
    aes(x=PC1, y=PC2)
  ) +
  geom_point(aes(colour=status)) +
  geom_text_repel(aes(label=sample)) +
  xlab('PC1') +
  ylab('PC2')
  
```

Based on Figure\ \@ref(fig:validation-pca-projection),
we say that classes are well separated.
